import express from 'express';
import path from 'path'
import openid from 'express-openid-connect';

const { auth, requiresAuth } = openid

import {getTourneys, getMatches, storeTourney, storeMatches, getTourneyInfo, editMatches, deleteTourney} from './extra/index.js'
import { generateMatches } from './extra/generator.js';
import { matchArrange, pointsTable, editedRows, errorCheck} from './extra/process.js';

const app = express();
app.use(express.urlencoded({
  extended: true
}))

const __dirname = path.resolve()
app.set("views", path.join(__dirname, "views"));
app.set('view engine', 'pug');

const externalUrl = process.env.RENDER_EXTERNAL_URL
const port = process.env.PORT

const config = { 
  authRequired : false,
  idpLogout : true,
  secret: process.env.SECRET,
  baseURL: externalUrl,
  clientID: process.env.CLIENT_ID,
  issuerBaseURL: 'https://dev-wqbopmiv8gxpavfu.eu.auth0.com',
  clientSecret: process.env.CLIENT_SECRET,
  authorizationParams: {
    response_type: 'code',
    scope: "openid profile email"   
   },
};

app.use(auth(config));


app.get('/',  requiresAuth(), async function (req, res) {
  const creator = req.oidc.user.email;
  const user = req.oidc.user.name;
  const userInfo = {
    creator: creator,
    user: user,
    tourneys: await getTourneys(creator)
  }
  res.render('home', {userInfo});
});

app.get('/create',  requiresAuth(), function (req, res) {
  const error = req.query.err || null
  res.render('create', {error});
});

app.post('/',  requiresAuth(), async function (req, res) {
  const errMsg = errorCheck(req.body)
  if (errMsg != null) {
    res.redirect('/create?err='+errMsg)
  } else {
    const tourney = {
      creator: req.oidc.user.email,
      name: req.body.name.substring(0,50),
      players: req.body.players.split("'").join('').split('"').join(''),
      win: parseFloat(req.body.win),
      draw: parseFloat(req.body.draw),
      lose: parseFloat(req.body.lose)
    }
    const generatedId = await storeTourney(tourney)
    tourney["id"] = generatedId
    const matches = generateMatches(tourney)
    const wait = await storeMatches(matches)
    res.redirect('/tournament?id='+generatedId)
  }
});

app.get('/tournament', async function (req, res) {
  const tourneyInfo = await getTourneyInfo(req.query.id)
  const matches = await getMatches(req.query.id)
  const matches2 = matchArrange(matches)
  const table = pointsTable(matches, tourneyInfo.win, tourneyInfo.draw, tourneyInfo.lose)
  const tourney = {
    id: req.query.id,
    owner: null,
    table: table,
    tourneyInfo: tourneyInfo,
    matches: matches2
  }
  if (req.oidc.isAuthenticated()) {
    const user = req.oidc.user.email;
    tourney.owner = user     
    res.render('tournament', {tourney});
  } else {
    res.render('tournament', {tourney});
  }
});

app.post('/edit',  requiresAuth(), async function (req, res) {
  const rows = editedRows(req.body)
  const promises = []
  rows.forEach(row => {
    promises.push(editMatches(row))
  })
  await Promise.all(promises)
  res.redirect('/tournament?id='+rows[0][0])
});

app.get('/delete',  requiresAuth(), async function (req, res) {
  const wait = await deleteTourney(req.query.id, req.oidc.user.email)
  res.redirect('/')
});

app.listen(port, '0.0.0.0');