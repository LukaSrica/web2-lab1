import pg from 'pg';
import dotenv from 'dotenv'
dotenv.config()

const {Pool} = pg

const pool = new Pool({
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: 'web2_lab1_gc15',
    password: process.env.DB_PASSWORD,
    port: 5432,
    ssl: true
})

export async function getTourneys(creator) {
    const results = await pool.query(`SELECT id, name FROM tournaments WHERE creator = '${creator}';`)
    return results.rows
}

export async function getMatches(id) {
    const results = await pool.query(`SELECT * FROM matches WHERE tourneyid = '${id}' ORDER BY game;`)
    return results.rows
}

export async function getTourneyInfo(id) {
    const results = await pool.query(`SELECT name, creator, win, draw, lose FROM tournaments WHERE id = '${id}';`)
    return results.rows[0]
}


export async function storeTourney(tourney) {
    let sql = 'INSERT INTO tournaments (creator, name, win, draw, lose) VALUES ($1,$2,$3,$4,$5) RETURNING id;'
    let params = [tourney.creator, tourney.name, tourney.win, tourney.draw, tourney.lose]
    const results = await pool.query(sql, params)
    return results.rows[0].id
}

export async function storeMatches(matches) {
    let query = 'INSERT INTO matches (tourneyid, round, game, player1, player2) VALUES '
    let rows = []
    matches.forEach(match => {
        rows.push(`(${match.tourneyID},${match.round}, '${match.game}', '${match.player1}','${match.player2}')`)
    });
    query += rows.join(',')
    query += ';'
    let wait = await pool.query(query)
}

export async function editMatches(row) {
    return new Promise(async function(resolve, reject) {
        let wait = await pool.query(`UPDATE matches SET score1 = ${row[2]}, score2 = ${row[3]} WHERE tourneyid = ${row[0]} AND game = ${row[1]}`)
        return resolve(wait)
    })
}

export async function deleteTourney(id, creator) {
    let wait = await pool.query(`DELETE FROM tournaments WHERE id = ${id} AND creator = '${creator}'`)
}