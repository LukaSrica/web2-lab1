export function matchArrange(matches) {
    const newMatches = []
    const rounds = matches[matches.length-1].round
    const gamesPerRound = matches.length / rounds
    for(let i=0; i < matches.length; i+=gamesPerRound) {
        newMatches.push(matches.slice(i, i+gamesPerRound))
    }
    return newMatches
}

export function pointsTable(matches, win, draw, lose) {
    const table = {}
    matches.forEach(match => {
        table[match.player1] = [0, 0, 0, 0]
        table[match.player2] = [0, 0, 0, 0]
    });
    matches.forEach(match => {
        if (match.score1 != null && match.score2 != null) {
            if (match.score1 > match.score2) {
                table[match.player1][0] += win
                table[match.player2][0] += lose
                table[match.player1][1] += 1
                table[match.player2][3] += 1
            } else if (match.score1 == match.score2) {
                table[match.player1][0] += draw
                table[match.player2][0] += draw
                table[match.player1][2] += 1
                table[match.player2][2] += 1
            } else {
                table[match.player1][0] += lose
                table[match.player2][0] += win
                table[match.player1][3] += 1
                table[match.player2][1] += 1
            }
        }
    });
    const sortable = Object.entries(table)
    const sorted = sortable.sort((a,b) => b[1][0] - a[1][0])
    return sorted
}

export function editedRows(rows) {
    const entries = Object.entries(rows)
    const editedRows = []
    for(let i=0; i < entries.length; i+=2) {
        if (entries[i][1] != '' && entries[i+1][1] != '') {
            const rowPk = entries[i][0].split('-')
            const temp = [parseInt(rowPk[0]), parseInt(rowPk[1]), parseInt(entries[i][1]), parseInt(entries[i+1][1])]
            editedRows.push(temp)
        }
    }
    return editedRows
}

export function errorCheck(req) {
    if (req.name.length < 1) {
        return '(Naziv natjecanja prazan)'
    }
    const players = req.players.split("'").join('').split('"').join('')
    const arrPlayers = players.split(';').filter(e => e != '')
    if (arrPlayers.length < 4 || arrPlayers.length > 8) {
        return '(Neispravan broj natjecatelja)'
    }
    const setPlayers = new Set(arrPlayers)
    if (arrPlayers.length != setPlayers.size) {
        return '(Više natjecatelja istog naziva)'
    }
    if (req.win == '' || req.draw == '' || req.lose == '') {
        return '(Sustav bodovanja nepotpun)'
    }
    return null
}