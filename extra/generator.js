export function generateMatches(tourney) {
    const matches = []
    const array = tourney.players.split(';')

    if (array.length % 2 != 0) {
        array.push(null)
    }
    const len = array.length
    const n = len / 2
    var game = 1

    const first = array.shift()

    for (let i=0; i < len-1; i++) {
        if (array[len-2] != null) {
            let match = {
                tourneyID: tourney.id,
                round: i+1,
                game: game,
                player1: first,
                player2: array[len-2],
                score: '-'
            }
            matches.push(match)
            game += 1
        }

        for (let j = 0; j < n-1; j++) {
            if (array[j] != null && array[len-3-j] != null) {
                let match = {
                    tourneyID: tourney.id,
                    round: i+1,
                    game: game,
                    player1: array[j],
                    player2: array[len-3-j]
                }
                matches.push(match)
                game += 1
            }
        }
        
        const toFront = array.pop()
        array.unshift(toFront)
    }
    return matches
}